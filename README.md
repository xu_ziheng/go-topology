# go-topology

#### 介绍
用 go 实现一组路由器算法，以发现和使用网络拓扑来有效地路由消息

#### 软件架构
软件架构说明

1.本项目会生成许多经典的网络拓扑并将路由器任务放置在选定的拓扑中。
它还运行一个简单的评估，将消息从每个路由器发送到每个其他路由器，并记录消息通过的跃点数。

2.单个路由器接受来自客户端的消息（以预定义的格式），该消息还包含目标 ID，期望这条消息最终会在这个 id 的路由器上结束，
跃点数最少，并且对网络拓扑中的路由异常具有鲁棒性，同时在目标路由器上处理客户端消息并将处理后消息返回给客户端（以预定义的格式）。

3.单个路由器可以连接到其他路由器的本地网络端口,单个路由器可以访问所有相邻路由器的ID，
以及路由器的总数。(在物理世界中，访问所有相邻路由器的ID需要首先通过初始通信来建立，但我们在这里假设这已经发生了)


#### 安装教程

go build -o router

#### 使用说明

router -t 拓扑类型 


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
