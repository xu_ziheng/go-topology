package routers

import (
	"fmt"
	"log"
	"strconv"
	"testing"
)

//type Node struct {
//	no int
//	length int
//	paths []int
//}

//func (n *Node) Less(other interface{}) bool {
//	return n.length < other.(*Node).length
//}

func TestMinPath(t *testing.T) {

	x := 3

	table := make([][]int, x)
	for i := range table {
		table[i] = make([]int, x)
		for j := 0; j < x; j++ {
			table[i][j] = -1
		}
	}

	pathtable := make([][][]int, x)

	for i := range pathtable {
		pathtable[i] = make([][]int, x)
	}

	table[0][1] = 1
	table[0][2] = 1

	table[1][0] = 1
	table[1][2] = 1

	table[2][0] = 1
	table[2][1] = 1

	for i := 0; i < x; i++ {
		caculateAndPrint(x, pathtable, table, i)
	}

}

func caculateAndPrint(x int, pathtable [][][]int, table [][]int, element int) {

	isconfirm := make([]bool, x)

	lenth := make([]int, x)

	for i := 0; i < len(lenth); i++ {
		lenth[i] = -1
	}

	priorityqueue := New()

	lenth[element] = 0

	priorityqueue.Push(&Node{
		no:     element,
		length: 0,
	})

	for {
		if priorityqueue.Len() == 0 {
			break
		}

		node := priorityqueue.Pop().(*Node)

		index := node.no

		length := node.length

		paths := node.paths

		isconfirm[index] = true

		for i := 0; i < len(table[index]); i++ {
			if table[index][i] > 0 && !isconfirm[i] {
				node1 := &Node{
					no:     i,
					length: length + table[index][i],
					paths:  append(paths, i),
				}
				log.Printf("lenth[i] : [%v]", lenth[i])
				log.Printf("node1.length : [%v]", node1.length)
				if lenth[i] == -1 || lenth[i] > node1.length {
					pathtable[element][i] = node1.paths
					lenth[i] = node1.length
					priorityqueue.Push(node1)
				}
			}

		}
	}

	//print result
	for i := 0; i < len(lenth); i++ {

		fmt.Print("source router： " + strconv.Itoa(element))
		fmt.Print("arrive router： " + strconv.Itoa(i))
		fmt.Print("router lenth： " + strconv.Itoa(lenth[i]))
		fmt.Print("router paths:")
		for _, item := range pathtable[element][i] {
			fmt.Print(item)
			fmt.Print(" ")
		}
		fmt.Println("")
	}

}

//基准测试
//go test -benchmem -bench .
func BenchmarkCaculate(b *testing.B){
	x := 3
	table := make([][]int, x)
	for i := range table {
		table[i] = make([]int, x)
		for j := 0; j < x; j++ {
			table[i][j] = -1
		}
	}
	pathtable := make([][][]int, x)
	for i := range pathtable {
		pathtable[i] = make([][]int, x)
	}
	table[0][1] = 1
	table[0][2] = 1
	table[1][0] = 1
	table[1][2] = 1
	table[2][0] = 1
	table[2][1] = 1
	b.ResetTimer()
	//for i:=0;i<b.N;i++ {
	//	caculateAndPrint(x,pathtable,table,0)
	//}
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			caculateAndPrint(x,pathtable,table,0)
		}
	})
}


