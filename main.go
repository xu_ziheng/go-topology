package main

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"math/rand"
	"os"
	"time"

	"routers"
)

var (
	//运行参数
	topology = flag.String("t", "Mesh", "`topology` (by size: Line, Ring, Star, Fully_Connected; "+
		"by dimension and size: Mesh, Torus; by dimension: Hypercube, Cube_Connected_Cycles, Butterfly, Wrap_Around_Butterfly)")
	size      = flag.Uint("s", 20, "size")
	dimension = flag.Uint("d", 2, "dimension")
	settleTime = flag.Duration("w", 10000*time.Millisecond, "routers settle time")
	mode     = flag.String("m", "One_To_All", "`mode` (One_To_All, All_To_One)")
	dropouts = flag.Uint("x", 0, "dropouts")
	repeats  = flag.Uint("r", 1, "repeats")
	force    = flag.Bool("f", false, "force the creation of a large number of routers")
	seed     = flag.Int64("p", 0, "seed for dropouts. 0 will drop routers from zero to number of dropouts. -1 will set a random seed")
	verbose  = flag.Bool("v", false, "print extra debugging information")
)

func main() {
	flag.Parse()

	if *seed == -1 {
		rand.Seed(time.Now().UnixNano())
	} else {
		rand.Seed(*seed)
	}

	if *size == 0 && (*topology == "Line" || *topology == "Ring" || *topology == "Star" || *topology == "Fully_Connected" ||
		*topology == "Mesh" || *topology == "Torus") {
		fmt.Fprintln(os.Stderr, "You have requested a topology with zero routers. Try increasing size (-s).")
		os.Exit(1)
	}
	//根据运行参数生成拓扑
	var template routers.Template
	switch *topology {
	case "Line", "Ring":
		template = make(routers.Template, *size)
		for i := routers.RouterId(0); uint(i) < *size; i++ {
			template[i] = []routers.RouterId{i - 1, i + 1}
		}
		if *topology == "Line" {
			template[0] = template[0][1:]
			template[*size-1] = template[*size-1][:len(template[*size-1])-1]
		} else {
			template[0][0] = routers.RouterId(*size - 1)
			template[*size-1][1] = 0
		}
	case "Star":
		template = make(routers.Template, *size)
		if *size > 0 {
			template[0] = make([]routers.RouterId, *size-1)
			for i := routers.RouterId(1); uint(i) < *size; i++ {
				template[0][i-1] = i
			}
			for i := uint(1); i < *size; i++ {
				template[i] = []routers.RouterId{0}
			}
		}
	case "Fully_Connected":
		template = make(routers.Template, *size)
		for i := routers.RouterId(0); uint(i) < *size; i++ {
			template[i] = make([]routers.RouterId, *size-1)
			for j := routers.RouterId(0); uint(j) < *size-1; j++ {
				if j < i {
					template[i][j] = j
				} else {
					template[i][j] = j + 1
				}
			}
		}
	case "Mesh":
		template = make(routers.Template, exp(*size, *dimension))
		for i := routers.RouterId(0); int(i) < len(template); i++ {
			temp := make(map[routers.RouterId]struct{})
			for d := uint(0); d < *dimension; d++ {
				if int(i)-(1<<d) >= 0 {
					temp[i-(1<<d)] = struct{}{}
				}
				if int(i)+(1<<d) < len(template) {
					temp[i+(1<<d)] = struct{}{}
				}
			}
			template[i] = make([]routers.RouterId, len(temp))
			j := 0
			for n := range temp {
				template[i][j] = n
				j++
			}
		}
	default:
		fmt.Fprintf(os.Stderr, "Unsupported topology %s\n", *topology)
		flag.Usage()
		os.Exit(1)
	}
	//初始化路由
	log.Println("Creating routers...")
	in, out := routers.MakeRouters(template)
	//等待路由初始化完成
	log.Println("Letting routers settle...")
	time.Sleep(*settleTime)

	if int(*dropouts) > len(in) {
		log.Fatalln("You have requested more dropouts than routers. Aborting tests.")
	} else if int(*dropouts) < 0 {
		log.Fatalln("You have either requested negative dropouts or triggered an overflow. Aborting tests.")
	}
	
	var shutdowns = make(map[routers.RouterId]struct{}, int(*dropouts))
	//测试某些路由节点关闭的情况
	if *dropouts > 0 {
		log.Println("Routers settled, commencing dropouts.")
		if *seed == 0 {
			for i := 0; i < int(*dropouts); i++ {
				shutdowns[routers.RouterId(i)] = struct{}{}
			}
		} else {
			for _, i := range rand.Perm(len(in))[0:int(*dropouts)] {
				shutdowns[routers.RouterId(i)] = struct{}{}
			}
		}

		for i := range shutdowns {
			if *verbose {
				log.Printf("Shutting down Router [%d]\n", i)
			}
			in[int(i)] <- routers.Shutdown{}
			close(in[int(i)])
		}
		log.Println("Beginning testing.")
	} else {
		log.Println("Routers settled, beginning testing.")
	}

	start := time.Now()

	msgs := make(map[uint]struct{})

	var (
		max_hops   uint
		total_hops uint
		successes  uint
	)
	//重复多少次
	for r := 0; r < int(*repeats); r++ {
		for m := routers.RouterId(*dropouts); int(m) < len(template); m++ {
			if _, ok := shutdowns[m]; ok {
				continue
			}
			for i := routers.RouterId(*dropouts); int(i) < len(template); i++ {
				if i == m {
					continue
				} else if _, ok := shutdowns[i]; ok {
					continue
				}
				msgs[uint(i)] = struct{}{}
				go func(j routers.RouterId) {
					switch *mode {
					case "One_To_All":
						in[m] <- routers.Envelope{
							Dest:    j,
							Hops:    0,
							Message: uint(j),
						}
					case "All_To_One":
						in[j] <- routers.Envelope{
							Dest:    m,
							Hops:    0,
							Message: uint(j),
						}
					default:
						fmt.Fprintf(os.Stderr, "Unsupported test mode %s\n", *mode)
						flag.Usage()
						os.Exit(1)
					}
				}(i)
			}
			//统计结果
			for {
				envelope := <-out
				if i, ok := envelope.Message.(uint); ok {
					if _, ok := msgs[i]; ok {
						if envelope.Hops > max_hops {
							max_hops = envelope.Hops
						}

						total_hops += envelope.Hops
						successes += 1

						delete(msgs, i)
						if len(msgs) == 0 {
							break
						}
					} else {
						log.Printf("Unexpected message value %v! Make sure you aren't duplicating Envelopes.", i)
					}
				} else {
					log.Printf("Unexpected message body %g! Make sure you aren't editing Envelopes.", envelope.Message)
				}
			}
		}
	}

	log.Printf("Test completed in %v\n", time.Since(start))
	log.Printf("Maximum hops: %d\n", max_hops)
	log.Printf("Average hops: %v\n", float64(total_hops)/float64(successes))
}

func exp(x uint, y uint) uint {
	z := new(big.Int).Exp(new(big.Int).SetUint64(uint64(*size)), new(big.Int).SetUint64(uint64(*dimension)), nil)
	if z.Cmp(big.NewInt(1024)) > 0 && !*force {
		fmt.Fprintf(os.Stderr, "Your chosen configuration would generate a very large number of routers.\n"+
			"Try setting dimension (-d) smaller. (Would generate %v routers.)\n"+
			"If you're really sure, you can use -f to proceed anyway.\n", z)
		os.Exit(1)
	}
	if !z.IsUint64() {
		fmt.Fprintln(os.Stderr, "OK, but seriously though, this would generate more than 2^64 routers. Aborting.")
		os.Exit(1)
	}
	return uint(z.Uint64())
}
